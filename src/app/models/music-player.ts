import { Howl, Howler } from 'howler';


export class MusicPlayer {
	private playlistURLs: string[];
	private playlist: Howl[];
	private index: number;
	constructor(urlList: string[]) {
		if(!Howler.codecs('mp3')) {
			console.error(`[Howler] Sorry, your browser does not support mp3 audio.`);
			return;
		}
		Howler.volume(0.8);
		this.index = 0;
		this.playlistURLs = urlList;
		this.playlist = this.playlistURLs.map(this.initHowl, this);
	}

	private initHowl(trackURL: string): Howl {
		const that = this;
		return new Howl({
			volume: 0.8,
			html5: true,
			xhrWithCredentials: true,
			src: [trackURL],
			preload: true,
			autoplay: false,
			onloaderror: () => {
				console.error(`[Howler] There has been an error loading the song.`);
				that.removeTrackSrc(that.index);
			},
			onplayerror: () => {
				console.error(`[Howler] There has been an error playing the song.`);
				that.removeTrackSrc(that.index);
			},
			onend: (songId: number) => {
				if((that.index < that.playlist.length - 1))  {
					that.index++;
					that.playlist[that.index].play();
				}
			}
		});
	}

	/** Information getting functions */
	public get songIndex(): number {
		return this.index;
	}
	public isPlaying(idx?: number): boolean {
		if((idx >= 0) && (idx < this.playlist.length)) return this.playlist[idx].playing();
		let currPlaying: boolean = false;
		this.playlist.forEach(song => currPlaying = currPlaying || song.playing());
		return currPlaying;
	}
	public get duration(): number {
		return this.playlist[this.index].duration();
	}

	/** Playlist functions */
	public getTrackSrc(): string {
		return this.playlistURLs[this.index];
	}
	public getTracklistSrc(): string[] {
		return this.playlistURLs;
	}
	public addTrackSrc(url: string): void {
		this.playlist.push(this.initHowl(url));
		this.playlistURLs.push(url);
	}
	public removeTrackSrc(idx: number) {
		if((idx >= 0) && (idx < this.playlist.length)) {
			this.playlist[idx].stop().unload();
			this.playlist.splice(idx, 1);
			this.playlistURLs.splice(idx, 1);
		}
	}

	/** Play controls functions */
	public play(idx?: number): number {
		this.playlist[this.index].stop();
		if((idx >= 0) && (idx < this.playlist.length)) this.index = idx;
		return this.playlist[this.index].play();
	}
	public togglePlay(): void {
		if(this.playlist[this.index].playing()) this.playlist[this.index].pause();
		else this.playlist[this.index].play();
	}
	public stop(): void {
		this.playlist[this.index].stop();
	}
	public next(): void {
		this.playlist[this.index].stop();
		if(this.index < (this.playlist.length - 1)) this.index++;
		this.playlist[this.index].play();
	}
	public prev(): void {
		this.playlist[this.index].stop();
		if(this.index > 0) this.index--;
		this.playlist[this.index].play();
	}
	public seek(time?: number): number | Howl {
		return this.playlist[this.index].seek(time);
	}

	/** Volume functions */
	public volumeUp(): void {
		if(Howler.volume() < 1) Howler.volume(Howler.volume() + 0.5);
	}
	public volumeDown(): void {
		if(Howler.volume() > 0) Howler.volume(Howler.volume() - 0.5);
	}

	/** Destructor */
	public destroy(): void {
		this.playlist.forEach(howl => howl.stop().unload());
		Howler.unload();
	}
}
