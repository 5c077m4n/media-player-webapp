export interface Song {
	name?: string;
	url?: string;
}
