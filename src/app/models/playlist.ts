import { Song } from "./song";


export interface Playlist {
	id?: number;
	name?: string;
	image?: string;
	songs?: Song[];
}
