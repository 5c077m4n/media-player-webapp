import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { SharedService } from '../../services/shared.service';
import { PlaylistService } from '../../services/playlist.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { Playlist } from '../../models/playlist';
import { MusicPlayer } from '../../models/music-player';
import { AddPlaylistComponent } from '../dialogs/add-playlist/add-playlist.component';
import { AddPlaylistTracklistComponent } from '../dialogs/add-playlist-tracklist/add-playlist-tracklist.component';


@Component({
	selector: 'app-now-playing',
	templateUrl: './now-playing.component.html',
	styleUrls: ['./now-playing.component.css'],
	animations: [
		trigger('playerState', [
			state('playing', style({transform: 'rotateZ(0)'})),
			state('paused', style({transform: 'rotateZ(0)'})),
			transition('* => playing', animate(
				20 * 60 * 1000,
				style({transform: `rotateZ(${40 * 360}deg)`})
			)),
			transition('playing => *', animate(300, style({transform: 'rotateZ(-360deg)'})))
		])
	]
})
export class NowPlayingComponent implements OnInit, OnDestroy, DoCheck {
	public playlist: Playlist;
	public audioPlayer: MusicPlayer;
	public playerState: string;
	private listener: Subscription;
	constructor(
		private messenger: SharedService,
		private playlistService: PlaylistService,
		private snackBar: SnackBarService,
		public dialog: MatDialog
	) {}
	ngOnInit() {
		this.listener = this.messenger.getObservable().subscribe(
			(pl: Playlist) => {
				if(pl && pl.id) {
					if(this.audioPlayer) this.audioPlayer.destroy();
					this.playlist = pl;
					this.playlistService.getPlaylistSongs(pl.id)
					.subscribe(res => {
						this.playlist.songs = res.data.songs;
						this.audioPlayer = new MusicPlayer(
							this.playlist.songs.map(song => song.url)
						);
					});
				}
			}
		);
	}
	ngDoCheck(): void {
		if(this.audioPlayer) {
			if(this.audioPlayer.isPlaying()) this.playerState = 'playing';
			else this.playerState = 'paused';
		}
	}

	private updatePlaylist(): void {
		this.playlistService.updatePlaylistInfo(
			this.playlist.id,
			this.playlist.name,
			this.playlist.image
		)
		.subscribe(res => {
			if(!(res && res.success)) {
				this.snackBar.show(`${this.playlist.name.toUpperCase()} has NOT been updated.`);
				return;
			}
			else {
				this.playlistService.updatePlaylistSongs(
					this.playlist.id, this.playlist.songs
				)
				.subscribe(res => {
					this.messenger.emit('refreshList');
					if(res && res.success)
						this.snackBar.show(`${this.playlist.name.toUpperCase()} has been updated.`);
				});
			}
		});
	}
	public editPlaylist(): void {
		const dialogRef = this.dialog.open(AddPlaylistComponent, {
			width: '80%',
			data: this.playlist
		});
		dialogRef.afterClosed().subscribe(playlist => {
			if(!(playlist && playlist.name && playlist.name.trim())) return;
			playlist.name = playlist.name.trim();
			if(playlist.image) playlist.image = playlist.image.trim();
			const dialogRef2 = this.dialog.open(AddPlaylistTracklistComponent, {
				width: '80%',
				data: this.playlist.songs
			});
			dialogRef2.afterClosed().subscribe(songList => {
				if(!(songList && songList.length)) return;
				this.playlist.songs = songList.filter(song => (song.name && song.url));
				this.updatePlaylist();
			});
		});
	}

	public onKeyPress(e) {
		switch(e.key) {
			case 'ArrowRight':
				this.audioPlayer.next();
				break;
			case 'ArrowLeft':
				this.audioPlayer.prev();
				break;
			case 'ArrowUp':
				this.audioPlayer.volumeUp();
				break;
			case 'ArrowDown':
				this.audioPlayer.volumeDown();
				break;
			case 'Enter':
				e.preventDefault();
				this.audioPlayer.togglePlay();
				break;
			case ' ':
				e.preventDefault();
				this.audioPlayer.togglePlay();
				break;
			case 'Escape':
				e.preventDefault();
				this.closeNowPlaying();
				break;
		}
	}

	public closeNowPlaying(): void {
		this.messenger.emit('hide-player');
	}
	ngOnDestroy(): void {
		this.listener.unsubscribe();
		delete this.playlist;
		this.audioPlayer.destroy();
	}
}
