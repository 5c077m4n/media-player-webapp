import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, endWith } from 'rxjs/operators';

import { Playlist } from '../../models/playlist';
import { PlaylistService } from '../../services/playlist.service';
import { SharedService } from '../../services/shared.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { AddPlaylistComponent } from '../dialogs/add-playlist/add-playlist.component';
import { AddPlaylistTracklistComponent } from '../dialogs/add-playlist-tracklist/add-playlist-tracklist.component';


@Component({
	selector: 'app-nav-bar',
	templateUrl: './nav-bar.component.html',
	styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
	public searchControl = new FormControl();
	private playlists: Playlist[] = [];
	public filteredPlaylists: Observable<Playlist[]>;
	constructor(
		private playlistService: PlaylistService,
		private messenger: SharedService,
		private snackBar: SnackBarService,
		public dialog: MatDialog
	) {}
	ngOnInit() {
		this.getPlaylists();
		this.filteredPlaylists = this.searchControl.valueChanges.pipe(
			startWith(''),
			map((value: string | Playlist) => (typeof value === 'string')? value : value.name),
			map(name => (name)? this.filterPlaylistsByName(name) : this.playlists.slice())
		);
	}
	private filterPlaylistsByName(term: string): Playlist[] {
		const filterValue = term.toLowerCase();
		return this.playlists.filter(
			pl => pl.name.toLowerCase().indexOf(filterValue) === 0
		);
	}
	private addPlaylist(playlist) {
		if(!(playlist && playlist.name)) return;
		this.playlistService.addPlaylist(playlist)
		.subscribe(res => {
			this.messenger.emit('refreshList');
			if(res && res.success)
				this.snackBar.show(`${playlist.name} has been added.`);
		});
	}

	public getPlaylists(): void {
		this.playlistService.getAllPlaylists().subscribe(
			list => this.playlists = list.data
		);
	}
	public displayFn(pl?: Playlist): string | undefined {
		return (pl)? pl.name : undefined;
	}
	public emitPlaylist(pl: Playlist) {
		this.messenger.emit(pl);
	}

	public openNewPlaylistDialog(): void {
		let newPlaylist: Playlist = { songs: [{}, {}, {}] };
		const dialogRef = this.dialog.open(AddPlaylistComponent, {
			width: '80%',
			data: newPlaylist
		});
		dialogRef.afterClosed().subscribe(playlist => {
			if(!(playlist && playlist.name && playlist.name.trim())) return;
			newPlaylist.name = playlist.name.trim();
			if(playlist.image) newPlaylist.image = playlist.image.trim();
			const dialogRef2 = this.dialog.open(AddPlaylistTracklistComponent, {
				width: '80%',
				data: newPlaylist.songs
			});
			dialogRef2.afterClosed().subscribe(songList => {
				if(!(songList && songList.length)) return;
				newPlaylist.songs = songList.filter(song => (song.name && song.url));
				this.addPlaylist(newPlaylist);
			});
		});
	}

	stopPropagation(e): void {
		e.stopPropagation();
	}
}
