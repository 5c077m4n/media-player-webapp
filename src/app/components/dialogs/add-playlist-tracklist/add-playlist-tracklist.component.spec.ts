import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlaylistTracklistComponent } from './add-playlist-tracklist.component';

describe('AddPlaylistTracklistComponent', () => {
  let component: AddPlaylistTracklistComponent;
  let fixture: ComponentFixture<AddPlaylistTracklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlaylistTracklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlaylistTracklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
