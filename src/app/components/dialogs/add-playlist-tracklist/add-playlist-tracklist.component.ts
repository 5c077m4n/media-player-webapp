import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormArray, Validators } from '@angular/forms';


@Component({
	templateUrl: './add-playlist-tracklist.component.html',
	styleUrls: ['./add-playlist-tracklist.component.css']
})
export class AddPlaylistTracklistComponent implements OnInit {
	public tracklistForm = this.formBuilder.group({
		songs: this.formBuilder.array([], Validators.required)
	});
	constructor(
		public dialogRef: MatDialogRef<AddPlaylistTracklistComponent>,
		private formBuilder: FormBuilder,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {}
	ngOnInit() {
		this.data.forEach(
			item => this.addSong(item.name, item.url)
		);
	}
	public get songForms(): FormArray {
		return this.tracklistForm.get('songs') as FormArray;
	}
	public addSong(name?: string, url?: string): void {
		this.songForms.push(
			this.formBuilder.group({
				name: [name],
				url: [url, [
					Validators.minLength(4),
					Validators.pattern(/\.(?:wav|mp3)$/i)
				]]
			})
		);
	}
	public deleteSong(indx: number): void {
		this.songForms.removeAt(indx);
	}

	close(): void {
		this.dialogRef.close();
	}
}
