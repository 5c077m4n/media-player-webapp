import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';


@Component({
	templateUrl: './add-playlist.component.html',
	styleUrls: ['./add-playlist.component.css']
})
export class AddPlaylistComponent implements OnInit {
	public playlistInfoForm = this.formBuilder.group({
		name: [this.data.name, Validators.minLength(3)],
		image: [this.data.image],
	}, Validators.required);
	constructor(
		public dialogRef: MatDialogRef<AddPlaylistComponent>,
		private formBuilder: FormBuilder,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {}
	ngOnInit() {}

	close(): void {
		this.dialogRef.close();
	}
}
