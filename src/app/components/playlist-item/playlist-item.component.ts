import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Playlist } from '../../models/playlist';
import { PlaylistService } from '../../services/playlist.service';
import { SharedService } from '../../services/shared.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { AddPlaylistComponent } from '../dialogs/add-playlist/add-playlist.component';
import { AddPlaylistTracklistComponent } from '../dialogs/add-playlist-tracklist/add-playlist-tracklist.component';


@Component({
	selector: 'app-playlist-item',
	templateUrl: './playlist-item.component.html',
	styleUrls: ['./playlist-item.component.css']
})
export class PlaylistItemComponent implements OnInit {
	@Input() public playlist: Playlist;
	constructor(
		private playlistService: PlaylistService,
		private messenger: SharedService,
		private snackBar: SnackBarService,
		public dialog: MatDialog
	) {}
	ngOnInit() {}

	public play() {
		window.setTimeout(() => this.messenger.emit('show-player'), 0);
		window.setTimeout(() => this.messenger.emit(this.playlist), 0);
	}

	private updatePlaylist(): void {
		this.playlistService.updatePlaylistInfo(
			this.playlist.id, this.playlist.name, this.playlist.image
		)
		.subscribe(res => {
			if(!(res && res.success)) return;
			else {
				this.playlistService.updatePlaylistSongs(
					this.playlist.id, this.playlist.songs
				)
				.subscribe(res => {
					this.messenger.emit('refreshList');
					if(res && res.success)
						this.snackBar.show(`${this.playlist.name.toUpperCase()} has been updated.`);
				});
			}
		});
	}
	public editPlaylist(): void {
		this.playlistService.getPlaylistSongs(this.playlist.id)
		.subscribe(res => this.playlist.songs = res.data.songs);

		const dialogRef = this.dialog.open(AddPlaylistComponent, {
			width: '80%',
			data: this.playlist
		});
		dialogRef.afterClosed().subscribe(playlist => {
			if(!(playlist && playlist.name && playlist.name.trim())) return;
			playlist.name = playlist.name.trim();
			if(playlist.image) playlist.image = playlist.image.trim();
			const dialogRef2 = this.dialog.open(AddPlaylistTracklistComponent, {
				width: '80%',
				data: this.playlist.songs
			});
			dialogRef2.afterClosed().subscribe(songList => {
				if(!(songList && songList.length)) return;
				this.playlist.songs = songList.filter(song => (song.name && song.url));
				this.updatePlaylist();
			});
		});
	}

	public deletePlaylist(): void {
		this.playlistService.deletePlaylist(this.playlist.id)
		.subscribe(res => {
			this.messenger.emit('refreshList');
			if(res && res.success)
				this.snackBar.show(`${this.playlist.name.toUpperCase()} has been deleted.`);
		});
	}
}
