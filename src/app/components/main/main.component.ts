import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { SharedService } from '../../services/shared.service';


@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
	public listener: Subscription;
	public showPlayer: boolean = false;
	constructor(private messenger: SharedService) {}
	ngOnInit() {
		this.listener = this.messenger.getObservable()
		.subscribe(res => {
			if(res === 'show-player') this.showPlayer = true;
			if(res === 'hide-player') this.showPlayer = false;
		});
	}

	ngOnDestroy(): void {
		this.listener.unsubscribe();
	}
}
