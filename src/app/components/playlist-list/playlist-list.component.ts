import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Playlist } from '../../models/playlist';
import { PlaylistService } from '../../services/playlist.service';
import { SharedService } from '../../services/shared.service';


@Component({
	selector: 'app-playlist-list',
	templateUrl: './playlist-list.component.html',
	styleUrls: ['./playlist-list.component.css']
})
export class PlaylistListComponent implements OnInit, OnDestroy {
	public playlists: Playlist[];
	private listner: Subscription
	constructor(
		private playlistService: PlaylistService,
		private messenger: SharedService
	) {}
	ngOnInit() {
		this.getAllPlaylists();
		this.listner = this.messenger.getObservable().subscribe(
			msg => (msg === 'refreshList')? this.getAllPlaylists() : undefined
		);
	}
	getAllPlaylists(): void {
		this.playlistService.getAllPlaylists()
		.subscribe(playlists => this.playlists = playlists.data);
	}

	ngOnDestroy(): void {
		this.listner.unsubscribe();
	}
}
