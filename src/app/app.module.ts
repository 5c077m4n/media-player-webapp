import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialStylesModule } from './modules/material-styles/material-styles.module';
import { AppComponent } from './app.component';
import { AddPlaylistComponent } from './components/dialogs/add-playlist/add-playlist.component';
import { AddPlaylistTracklistComponent } from './components/dialogs/add-playlist-tracklist/add-playlist-tracklist.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { PlaylistItemComponent } from './components/playlist-item/playlist-item.component';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { NowPlayingComponent } from './components/now-playing/now-playing.component';
import { MainComponent } from './components/main/main.component';


@NgModule({
	declarations: [
		AppComponent,
		AddPlaylistComponent,
		AddPlaylistTracklistComponent,
		NavBarComponent,
		PlaylistItemComponent,
		PlaylistListComponent,
		NowPlayingComponent,
		MainComponent
	],
	imports: [
		HttpClientModule,
		BrowserModule,
		MaterialStylesModule,
		FlexLayoutModule,
		FormsModule,
		ReactiveFormsModule
	],
	entryComponents: [
		AddPlaylistComponent,
		AddPlaylistTracklistComponent
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
