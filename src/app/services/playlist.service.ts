import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, retryWhen, delay, take } from 'rxjs/operators';

import { Response } from '../models/response';
import { Playlist } from '../models/playlist';
import { Song } from '../models/song';


@Injectable({ providedIn: 'root' }) export class PlaylistService {
	private readonly httpOptions;
	private readonly URL: string = '/api';
	constructor(private http: HttpClient) {
		this.httpOptions = {
			headers: new HttpHeaders({
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			})
		};
	}

	getAllPlaylists(): Observable<Response<Playlist[]>> {
		return this.http.get<Response<Playlist[]>>(
			`${this.URL}/playlist`, this.httpOptions
		)
		.pipe(
			tap((response: Response<Playlist[]>) => {
				if(response && response.data) console.log(`[PlaylistService] Playlists recieved.`);
				else console.error(`[PlaylistService] There was an error in getting the playlists.`);
			}),
			catchError(this.handleError('getAllPlaylists'))
		);
	}
	getPlaylist(id: number): Observable<Response<Playlist>> {
		return this.http.get<Playlist>(
			`${this.URL}/playlist/${id}`, this.httpOptions
		)
		.pipe(
			tap((response: Response<Playlist>) => {
				if(response && response.success) console.log(`[PlaylistService] Playlist recieved.`);
				else console.error(`[PlaylistService] There was an error in getting the playlist.`);
			}),
			catchError(this.handleError('getPlaylist'))
		);
	}
	getPlaylistSongs(id: number): Observable<any> {
		return this.http.get<any>(
			`${this.URL}/playlist/${id}/songs`, this.httpOptions
		)
		.pipe(
			tap((response: any) => {
				if(response && response.success) console.log(`[PlaylistService] Playlist's tracklist recieved.`);
				else console.error(`[PlaylistService] There was an error in getting the playlist's songs.`);
			}),
			catchError(this.handleError('getPlaylistSongs'))
		);
	}
	addPlaylist(playlist: Playlist): Observable<Response<Playlist>> {
		return this.http.post<Response<Playlist>>(
			`${this.URL}/playlist`,
			playlist,
			this.httpOptions
		)
		.pipe(
			tap((response: Response<Playlist>) => {
				if(response && response.success) console.log(`[PlaylistService] Playlist posted.`);
				else console.error(`[PlaylistService] There was an error in posting your playlist.`);
			}),
			catchError(this.handleError('addPlaylist'))
		);
	}
	updatePlaylistInfo(id: number, ...args: any[]): Observable<any> {
		return this.http.post<Response<any>>(
			`${this.URL}/playlist/${id}`,
			{...args},
			this.httpOptions
		)
		.pipe(
			tap((response: any) => {
				if(response && response.success) console.log(`[PlaylistService] Playlist posted.`);
				else console.error(`[PlaylistService] There was an error in posting your playlist.`);
			}),
			catchError(this.handleError('updatePlaylistInfo'))
		);
	}
	updatePlaylistSongs(id: number, ...args: any[]): Observable<any> {
		return this.http.post<Response<any>>(
			`${this.URL}/playlist/${id}/songs`, {...args}, this.httpOptions
		)
		.pipe(
			tap((response: any) => {
				if(response && response.success) console.log(`[PlaylistService] Playlist posted.`);
				else console.error(`[PlaylistService] There was an error in posting your playlist.`);
			}),
			catchError(this.handleError('updatePlaylistSongs'))
		);
	}
	deletePlaylist(id: number): Observable<Response<void>> {
		return this.http.delete<any>(
			`${this.URL}/playlist/${id}`,
			this.httpOptions
		)
		.pipe(
			tap((response: Response<void>) => {
				if(response && response.success) console.error(`[PlaylistService] Playlist deleted.`);
				else console.log(`[PlaylistService] There was an error deleting the playlist.`);
			}),
			catchError(this.handleError('getPlaylist'))
		);
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(`[SchoolService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
