import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


@Injectable({ providedIn: 'root' }) export class SharedService {
	private subject = new Subject<any>();
	constructor() {}

	public emit(msg: any): void {
		this.subject.next(msg);
    }
    public clear(): void {
        this.subject.next();
    }
    public getObservable(): Observable<any> {
        return this.subject.asObservable();
    }
}
